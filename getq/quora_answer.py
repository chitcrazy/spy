#-*- coding:utf-8 –*-
#

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import os
import time
import json
import optparse

from bs4 import BeautifulSoup
from quora_login import login
import config
import setup

def dump_answer_list_from_question(driver, question, qurl):
    time.sleep(0.5)
    driver.get("https://www.quora.com" + qurl)
    time.sleep(0.5)
    answer_list_div = driver.find_element_by_class_name("AnswerPagedList")
    src_updated = answer_list_div.get_attribute("innerHTML")
    src = ""
    while src != src_updated:
        src = src_updated
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(2)
        answer_list_div = driver.find_element_by_class_name("AnswerPagedList")
        src_updated = answer_list_div.get_attribute("innerHTML")

    soup = BeautifulSoup(src)
    all_answer_info = soup.find_all("div", "Answer")
    answers = []

    for ai in all_answer_info:
        answer_text = ""
        answer_t = ai.find("span", "inline_editor_value")
        if not answer_t:
            continue
        tag1 = answer_t.find("div", "AnswerFooter")
        tag2 = answer_t.find("div", "OriginallyAnsweredBanner")
        if tag1 != None:
            tag1.clear()
        if tag2 != None:
            tag2.clear()
        answer_text = answer_t.get_text(" ")
        if len(answer_text) > 30:
            answers.append(answer_text)
    return (qurl, answers)

def dump_answer_list_from_question_list(driver, questions):
    for (q, url) in questions:
        answers = dump_answer_list_from_question(driver, q, url)
        write_answer_list_into_json_file(config.answer_dir, url, answers)

def write_answer_list_into_json_file(fullfn, answers):
    if not fullfn:
        return False
    if os.path.isfile(fullfn):
        print('[W] file %s already exists' % fullfn)
        return False
    with open(fullfn, 'w', encoding='utf-8') as json_out:
        json.dump(answers, json_out, indent=2)

def read_question_list_from_json_file(fullfn):
    with open(fullfn, 'r', encoding='utf-8') as json_in:
        questions = json.load(json_in)
    return questions

def print_answer_list(question_url, answers):
    print('Dump question from %s' % question_url)
    for answer in answers:
        print("-----------")
        print(answer)

def scrape_answer_list():
    driver = webdriver.Chrome('chromedriver')
    if login(driver, config.username, config.password):
        for fin_fn in os.listdir(config.question_dir):
            if fin_fn.endswith("json"):
                print('read from: %s' % fin_fn)
                questions = read_question_list_from_json_file(config.question_dir + '\\' + fin_fn)
                if config.debug:
                    print(questions)
                answers = []
                for (q, qurl) in questions:
                    ss = qurl[1:]
                    ss = ss.split('?')[0]
                    fout_name = config.answer_dir + "\\" + ss + '.json'
                    if os.path.isfile(fout_name):
                        print("[I] The question has been grabbed, SKIP (%s)" % qurl)
                        continue
                    (qurl, answers) = dump_answer_list_from_question(driver, q, qurl)
                    write_answer_list_into_json_file(fout_name, answers)

if __name__ == '__main__':
    setup.mkdirs()
    parser = optparse.OptionParser()
    parser.add_option("-l", "--loop", action="store_true", dest = 'loop')
    parser.add_option("-d", "--debug", action="store_true", dest = 'debug')
    (options, args) = parser.parse_args()
    if options.debug:
        config.debug = options.debug
    if options.loop:
        print('[I] Enter infinite loop')
        while 1:
            scrape_answer_list()
            print('[I] Next loop')
    else:
        scrape_answer_list()