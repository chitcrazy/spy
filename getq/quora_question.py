#-*- coding:utf-8 –*-

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By

import os
import time
import json
import optparse

from bs4 import BeautifulSoup

from quora_login import login
import config
import setup

def dump_question_list_from_topic(driver, topic, url):
    time.sleep(0.5)
    driver.get("https://www.quora.com" + url + "/top_stories")
    time.sleep(0.5)
    question_list_div = driver.find_element_by_class_name("Feed")
    src_updated = question_list_div.get_attribute("innerHTML")
    src = ""
    k = 0
    while k < 3:
        if src == src_updated:
            k = k + 1
        else:
            src = src_updated
            k = 0
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(3)
        question_list_div = driver.find_element_by_class_name("Feed")
        src_updated = question_list_div.get_attribute("innerHTML")

    soup = BeautifulSoup(src)
    question_list_info = soup.find_all("a", "question_link")
    questions = []
    for qi in question_list_info:
        question_url = qi.get("href")
        question_url = question_url.split('?')[0]
        question_text = qi.get_text()
        if not question_url:
            continue
        questions.append((question_text, question_url))
    return questions

def write_question_list_into_json_file(fullfn, questions):
    if not fullfn:
        return False
    if os.path.isfile(fullfn):
        print('[W] file %s already exists' % fullfn)
        return False
    with open(fullfn, 'w', encoding='utf-8') as json_out:
        json.dump(questions, json_out, indent=2)

def read_topic_list_from_json_file(fullfn):
    with open(fullfn, 'r', encoding='utf-8') as json_in:
        topics = json.load(json_in)
    return topics

def scrape_question_list():
    print('[I] Start web browser ...')
    driver = webdriver.Chrome('chromedriver')
    if login(driver, config.username, config.password):
        for fin_fn in os.listdir(config.topic_dir):
            if fin_fn.endswith("json"):
                print('[I] read from: %s' % fin_fn)
                topics = read_topic_list_from_json_file(config.topic_dir + '\\' + fin_fn)
                for (tp, turl) in topics:
                    fout_name = config.question_dir + "\\" + tp + '.json'
                    if os.path.isfile(fout_name):
                        print("[I] The question file has been grabbed, SKIP (%s)" % turl)
                        continue
                    questions = dump_question_list_from_topic(driver, tp, turl)
                    write_question_list_into_json_file(fout_name, questions)

if __name__ == '__main__':
    setup.mkdirs()
    parser = optparse.OptionParser()
    parser.add_option("-l", "--loop", action="store_true", dest = 'loop')
    parser.add_option("-d", "--debug", action="store_true",dest = 'debug')
    (options, args) = parser.parse_args()
    if options.debug:
        config.debug = options.debug
    if options.loop:
        print('[I] Enter infinite loop')
        while 1:
            scrape_question_list()
            print('[I] Next loop')
    else:
        scrape_question_list()
