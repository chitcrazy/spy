# -*- coding:utf-8 –*-
# Article-scraper for Quora.com

# Author: Hao Chen, Yu Guo

# Log:
#  [20151115] author: log
#  [20151114] guoyu: add code of grabbing all topics from quora.com

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By

import bs4
from bs4 import BeautifulSoup

import os
import time
import json
import optparse

import config
import setup

# TODO: add more code for detecting platform automatically.
#       We are supposed to support Linux, Mac and Win7.

# Windows 7
#   guoyu: please make sure that chromedriver.exe is put in %PATH%

from utils import get_next_filename
from quora_login import login

global_topic_list = []


def dump_related_topics_from_topic(driver, topic):

    (tn, new_url) = topic
    try: 
        driver.get("http://www.quora.com" + new_url)
        print("[D] Start dump topics from %s" % tn)

        time.sleep(1)

        try:
            WebDriverWait(driver, 5).until(lambda x: x.find_element_by_class_name("expand_link"))
            elink = driver.find_element_by_class_name("expand_link")
            if config.debug:
                print("[D] Click View More. the link id = {0}".format(elink.get_attribute("id")))
                print("[D] The element is {}".format(elink.tag_name))
            elink.click()
            # wait for that more topics have been loaded
            if config.debug:
                print('[D] Topics has been expanded.')
            WebDriverWait(driver, 5).until_not(lambda x: x.find_element_by_class_name("expand_link"))
        except NoSuchElementException:
            print("[W] element not found <expand_link>")
        except TimeoutException:
            print("[W] element wait timeout <expand_link>")
        finally:
            pass

        if config.debug:
            print('[D] Topic list has been expanded.')
        time.sleep(1)

        try:
            if config.debug:
                print('[D] Wait for div with RelatedTopicsSection')
            WebDriverWait(driver, 5).until(lambda x: x.find_element_by_class_name("RelatedTopicsSection"))
            div_topic = driver.find_element_by_class_name("RelatedTopicsSection")
        except NoSuchElementException:
            print("[E] couldn't find the div of RelatedTopicsSection when dump %s" % tn)
            return []
        except TimeoutException:
            print("[W] element wait timeout <expand_link>")
            return []
        finally:
            pass

        if config.debug:
            print('[D] Topic list is ready for dumping.')

        topics_html = div_topic.get_attribute("innerHTML")
        soup = BeautifulSoup(topics_html)
        all_topic_info = soup.find_all("a", "RelatedTopicsListItem")

        url_list = []
        for ti in all_topic_info:
            topic_url = ti.get("href")
            topic_name = ti.find("span", "TopicName").get_text()
            url_list.append((topic_name, topic_url))
        if config.debug:
            print('[D] Get %d topics' % len(url_list))
        return url_list

    except NoSuchElementException as e:
        print("[E] Element not found: %s when dump %s " % (e, tn))

    return []


def dump_all_topics(driver, num: int, init_topic: str):
    topic_queue = [(init_topic, '/topic/'+init_topic)]
    topic_old_list = []
    while len(global_topic_list) < num:
        for t in topic_queue:
            if not (t in global_topic_list):
                global_topic_list.append(t)
        nq = []
        for t in topic_queue:
            if t in topic_old_list:
                continue
            ts = dump_related_topics_from_topic(driver, t)
            topic_old_list.append(t)
            for x in ts:
                if not (x in global_topic_list):
                    global_topic_list.append(x)
            if len(global_topic_list) > num:
                break
            nq = nq + ts
        topic_queue = nq
    return


def write_topics_into_file(filename, topics):
    try: 
        f = open(filename, "a")

        print("[I] writing into %s" % filename)

        for t in topics:
            (tn, tu) = t
            newline = tn + " " + tu + "\r\n"
            print("[I] write %s" % newline)
            f.writelines(newline)
    except IOError as e:
        print("[E] File IO error: %s" % e)
    finally:
        f.close()


def write_topics_into_json_file(fullfn, topics):
    print("[I] Writing into %s" % fullfn)
    with open(fullfn, "w", encoding='utf-8') as json_out:
        json.dump(topics, json_out, indent=2)
    print("[I] %d topics were written" % len(topics))


initial_topic = 'Book'
number_of_topics = 50


def scrape_topic_list():
    print('[I] Start web browser ...')
    driver = webdriver.Chrome('chromedriver.exe')
    if login(driver, config.username, config.password):
        print("[I] Start scraping ")
        dump_all_topics(driver, number_of_topics, initial_topic)
        print("[I] Get %d topics " % number_of_topics)
        if config.debug:
            print(global_topic_list)
        fn = get_next_filename("quora_topics_", config.topic_dir)
        fullfn = config.topic_dir + "/" + fn + ".json"
        write_topics_into_json_file(fullfn, global_topic_list)

if __name__ == '__main__':
    setup.mkdirs()
    parser = optparse.OptionParser()
    parser.add_option("-l", "--loop", action="store_true", dest='loop')
    parser.add_option("-n", "--number", type="int", dest='num')
    parser.add_option("-t", "--topic", type="string", dest='topic')
    (options, args) = parser.parse_args()
    if options.num:
        number_of_topics = options.num
    if options.topic:
        initial_topic = options.topic
    if options.loop:
        print('[I] Enter infinite loop')
        while 1:
            scrape_topic_list()
            print('[I] Next loop')
    else:
        scrape_topic_list()
