#-*- coding:utf-8 –*-

import selenium
from selenium import webdriver
import selenium.common.exceptions
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By

import time


def login(driver, username, password, try_times=3) -> bool:
    driver.get("http://www.quora.com")
    for i in range(try_times):
        try:
            input_field_usr = driver.find_element_by_css_selector("input[placeholder=\"Email\"]")

            input_field_usr.send_keys(username)
            print("[D] send username {}".format(username))

            input_field_pwd = driver.find_element_by_css_selector("input[placeholder=\"Password\"]")
            input_field_pwd.send_keys(password)
            print("[D] send password {}".format(password))

            input_button_login = driver.find_element_by_css_selector("input[value=\"Login\"]")
            time.sleep(1)
            input_button_login.click()

            div_banner = driver.find_elements_by_class_name("InteractionModeBanner")
            time.sleep(1)

            if not div_banner:
                print("[E] Login failed")
                return False
        except selenium.common.exceptions.WebDriverException as e:
            print(str(e))
            print("[E] Login failed (exception)")
            continue

        print("[I] login successfully")
        return True
