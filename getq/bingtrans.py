#-*- coding:utf-8 –*-

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0

import time
import os
import shutil
import sys
import json
import optparse

import config
import setup

def trans_answer(driver, answer) -> str:
    driver.get("http://www.bing.com/translator/?mkt=zh-CN")
    field = driver.find_element_by_id("InputText")
    field.send_keys(answer)
    time.sleep(1)
    field1 = driver.find_element_by_id("OutputArea")
    translated_text = field1.text
    print(translated_text)
    print("-----------------------------------")
    return translated_text

def read_answers_from_json_file(fn):
    with open(fn, 'r', encoding='utf-8') as json_in:
        answers = json.load(json_in)
        return answers

def write_transs_into_json_file(fn, transs):
    with open(fn, 'w', encoding='utf-8') as json_out:
        json.dump(transs, json_out, indent=2)
        return

def write_trans_into_txt_file(fn, trans):
    with open(fn, 'a', encoding='utf-8') as txt_out:
        txt_out.writelines(trans+'\r\n')

def write_trans_list_into_txt_file(fn, transs):
    with open(fn, 'w', encoding='utf-8') as txt_out:
        for t in transs:
            txt_out.writelines(t+'\r\n')

def run_translation():
    print('[I] Start web browser ...')
    driver = webdriver.Chrome('chromedriver')
    for fin_fn in os.listdir(config.answer_dir):
        if fin_fn.endswith('json'):
            fout_fn = config.trans_dir + '\\' + os.path.splitext(fin_fn)[0] + '.txt'
            if os.path.isfile(fout_fn):
                print('[I]  翻译file exists, SKIP (%s)' % fout_fn)
                continue
            print('[I] read from: %s ' % fin_fn)
            answers = read_answers_from_json_file(config.answer_dir + '\\' + fin_fn)
            transs = []
            for a in answers:
                t = trans_answer(driver, a)
                transs.append(t)
            print('[I] convert to: %s ' % fout_fn)
            write_trans_list_into_txt_file(fout_fn, transs)

if __name__ == '__main__':
    setup.mkdirs()
    parser = optparse.OptionParser()
    parser.add_option("-l", "--loop", action="store_true", dest = 'loop')
    parser.add_option("-d", "--debug", action="store_true", dest = 'debug')
    (options, args) = parser.parse_args()
    if options.debug:
        config.debug = options.debug
    if options.loop:
        print('[I] Enter infinite loop')
        while 1:
            run_translation()
            print('[I] Next loop')
    else:
        run_translation()

