#-*- coding:utf-8 –*-

import os

import config

def mkdirs():
    if not os.path.exists(config.answer_dir):
        os.makedirs(config.answer_dir)
        print('[I] Create dir %s' %  config.answer_dir)
    if not os.path.exists(config.trans_dir):
        os.makedirs(config.trans_dir)
        print('[I] Create dir %s' %  config.trans_dir)
    if not os.path.exists(config.topic_dir):
        os.makedirs(config.topic_dir)
        print('[I] Create dir %s' %  config.topic_dir)
    if not os.path.exists(config.question_dir):
        os.makedirs(config.question_dir)
        print('[I] Create dir %s' %  config.question_dir)

