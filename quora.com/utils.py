import os
import time
import re

def get_next_filename(fn, dir):
    max = -1
    for file in os.listdir(dir):  
        nfn = os.path.splitext(file)[0]
        match = re.match(r"([a-z_]+)([0-9]+)", nfn, re.I)
        if not match:
            continue
        (xfn, nstr) = match.groups()
        print(xfn,nstr)
        if fn == xfn:
            i = int(nstr)
            if i > max:
                max = i
    max = max + 1
    filename = fn + "%03d" % max
    return filename
