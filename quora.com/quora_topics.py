# Article-scraper for Quora.com

# Author: Hao Chen, Yu Guo

# Log:
#  [20151115] author: log
#  [20151114] guoyu: add code of grabbing all topics from quora.com

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By

from bs4 import BeautifulSoup

import os
import time
import re
import json

# TODO: add more code for detecting platform automatically.
#       We are supposed to support Linux, Mac and Win7.

# Windows 7
#   guoyu: please make sure that chromedriver.exe is put in %PATH%

from utils import get_next_filename
from quora_login import login



global_topic_list = []

def dump_related_topics_from_topic(topic):
    (tn, new_url) = topic
    try: 
        driver.get("http://www.quora.com" + new_url)
        print("[D] get topics from %s" % tn)

        time.sleep(1)

        try:
            WebDriverWait(driver, 5).until(lambda x: x.find_element_by_class_name("expand_link"))
            elink = driver.find_element_by_class_name("expand_link")
            print("click View More. the link id = {0}".format(elink.get_attribute("id")))
            print("the element is {}".format(elink.tag_name))
            elink.click()
            # wait for that more topics have been loaded
            WebDriverWait(driver, 10).until_not(lambda x: x.find_element_by_class_name("expand_link"))
        except NoSuchElementException:
            print("[D] element not found <expand_link>")            
        except TimeoutException:
            print("[W] element wait timeout <expand_link>")                        
        finally:
            pass

        time.sleep(1)

        try:
            WebDriverWait(driver, 5).until(lambda x: x.find_element_by_class_name("RelatedTopicsSection"))
            div_topic = driver.find_element_by_class_name("RelatedTopicsSection")
        except NoSuchElementException:
            print("[E] couldn't find the div of RelatedTopicsSection when dump %s" % tn)
            return []
        except TimeoutException:
            print("[W] element wait timeout <expand_link>")
            return []
        finally:
            pass

        time.sleep(1)
        
        topics_html = div_topic.get_attribute("innerHTML")
        soup = BeautifulSoup(topics_html)
        all_topic_info = soup.find_all("a", "RelatedTopicsListItem")

        url_list = []
        for ti in all_topic_info:
            topic_url = ti.get("href")
            topic_name = ti.find("span", "TopicName").get_text()
            url_list.append((topic_name, topic_url))
        return url_list

    except NoSuchElementException as e:
        print("[E] Element not found: %s when dump %s " % (e, tn))

    finally:
        pass

    return []

def dump_all_topics(num, init_topic):
    i = 0
    topic_queue = [(init_topic, '/topic/'+init_topic)]
    topic_old_list = []
    while (len(global_topic_list) < num):
        for t in topic_queue:
            if not (t in global_topic_list):
                global_topic_list.append(t)
        nq = []
        for t in topic_queue:
            if t in topic_old_list:
                continue
            ts = dump_related_topics_from_topic(t)
            topic_old_list.append(t)
            for x in ts:
                if not (x in global_topic_list):
                    global_topic_list.append(x)
            if (len(global_topic_list) < num):
                break
            print("Get topics from %s" % t[0])
            print(">>>>>>>")
            print(ts)
            print("<<<<<<<")
            print("New global list")
            print("--------------------->>>>>>>")
            print(global_topic_list)
            print("---------------------<<<<<<<")
            nq = nq + ts
        topic_queue = nq
    return
    

dump_file = "output.txt"

def write_topics_into_file(filename, topics):

    try: 
        f = open(filename, "a")

        print("[I] writing into %s" % dump_file)

        for t in topics:
            (tn, tu) = t
            newline = tn + " " + tu + "\r\n"
            print("[I] write %s" % newline)
            f.writelines(newline)
    except IOError as e:
        print("[E] File IO error: %s" % e)
    finally:
        f.close()

def write_topics_into_json_file(dir, fn, topics):          

    filename = get_next_filename(fn, dir)
    fullfn = filename+".json"
    print("[I] writing into %s" % fullfn)
    with open(dir + "/" + fullfn, "w", encoding='utf-8') as json_out:
        json.dump(topics, json_out, indent=2)

driver = webdriver.Chrome('chromedriver.exe')

username = "1412597721@qq.com"

password = "quora"

login(driver, username, password)

dump_all_topics(10, 'Google')

print(global_topic_list)

write_topics_into_json_file("data", "quora_topics_", global_topic_list)
