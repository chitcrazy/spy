#
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import time
import json

from bs4 import BeautifulSoup
from quora_login import login
from utils import get_next_filename

# Create a new instance of the Firefox driver
# driver = webdriver.Firefox()

driver = webdriver.Chrome('chromedriver') 

usr = "1412597721@qq.com"
pwd = "quora"

def dump_all_answers_from_question(driver, answer, url):
   time.sleep(0.5)
   driver.get("https://www.quora.com" + url)
   time.sleep(0.5)
   answer_list_div = driver.find_element_by_class_name("AnswerPagedList")
   src_updated = answer_list_div.get_attribute("innerHTML")
   src = ""
   while src != src_updated:
      src = src_updated
      driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
      time.sleep(2)
      answer_list_div = driver.find_element_by_class_name("AnswerPagedList")
      src_updated = answer_list_div.get_attribute("innerHTML")

   soup = BeautifulSoup(src)
   all_answer_info = soup.find_all("div", "Answer")
   answers = []
   for ai in all_answer_info:
      answer_t = ai.find("span", "inline_editor_value")
      if not answer_t:
         continue
      answer_text = answer_t.get_text()
      answers.append(answer_text)
   return answers

login(driver, usr, pwd)

time.sleep(0.5)

url = "/Should-I-get-the-iPhone-6-or-wait-for-the-6s"

answers = dump_all_answers_from_question(driver, "x", url)

def write_answers_into_json_file(dir, fn, answers): 
    filename = get_next_filename(fn, dir)
    fullfn = filename+".json"
    print("[I] writing into %s" % fullfn)
    with open(dir+"/"+fullfn, "w", encoding='utf-8') as json_out:
        json.dump(answers, json_out, indent=2)

write_answers_into_json_file("data", "quora_answers_", (url, answers))


