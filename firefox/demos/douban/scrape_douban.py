#-*- coding:utf-8 –*-

# Author: Hao Chen

# [Logs]
#     guoyu: this file is for python3

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
import time


# Create a new instance of the Firefox driver
driver = webdriver.Firefox()

# go to the douban home page
driver.get("http://www.douban.com/")

# find the link going to the move page 
ele1 = driver.find_element_by_class_name("lnk-movie")
ele1.click()

s="速度与激情"
s1="狂野时速7(港)"

field = driver.find_element_by_id("inp-query")
field.clear()
field.send_keys(s)
    
search = driver.find_element_by_class_name("inp-btn")
search.submit()

time.sleep(3)

print(s1)
p1 = driver.find_element_by_partial_link_text(s1)
p1.click()


time.sleep(3)

p2 = driver.find_element_by_id("hot-comments")
print(p2.text)
