# Chrome Web Driver for Selenium 简介

Update: 2015-11-14

Authors: 

* Hao Chen <>
* Yu  Guo <macdata@qq.com>

## Installation on Windows 7

**下载地址**: [https://sites.google.com/a/chromium.org/chromedriver/downloads](https://sites.google.com/a/chromium.org/chromedriver/downloads)

+ 下载 http://chromedriver.storage.googleapis.com/index.html?path=2.20/
+ 解压 chromedriver_win32.zip
+ 将 chromedriver.exe 放到系统环境变量 %PATH% 中
+ 请确保 Chrome 已经被安装到以下目录: `C:\Users\%USERNAME%\AppData\Local\Google\Chrome\Application\chrome.exe`, 详细信息请参考[cdwiki]

[cdwiki]:http://code.google.com/p/selenium/wiki/ChromeDriver

## Official Website

[https://sites.google.com/a/chromium.org](https://sites.google.com/a/chromium.org)




